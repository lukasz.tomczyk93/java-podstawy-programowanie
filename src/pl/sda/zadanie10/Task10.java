package pl.sda.zadanie10;

import java.util.Scanner;

class Task10 {

    public static void main(String[] args) {

        var scanner = new Scanner(System.in);
        System.out.print("Provide a positive number: ");
        int number = scanner.nextInt();

        if (number <= 0) {
            System.out.println("Not a positive number");
            return;
        }

        int sumOfDigitValues = getSumOfDigitValuesUsingModulo(number);

        System.out.println(String.format("Sum of digits: %s", sumOfDigitValues));
    }

    private static int getSumOfDigitValuesUsingCharArray(int number) {
        String numberAsString = String.valueOf(number);
        char[] digits = numberAsString.toCharArray();

        int sumOfDigitValues = 0;
        for (char digit : digits) {
            int digitAsInt = Character.getNumericValue(digit);
            sumOfDigitValues += digitAsInt;
        }
        return sumOfDigitValues;
    }

    private static int getSumOfDigitValuesUsingModulo(int number) {
        int digit;
        int mainPart = number;
        int sum = 0;

        while (mainPart > 0) {
            digit = mainPart % 10;
            sum += digit;

            mainPart = mainPart / 10;
        }

        return sum;
    }
}