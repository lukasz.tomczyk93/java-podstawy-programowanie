package pl.sda.zadanie4;

class SdaNumber {

    private final Integer value;

    SdaNumber(Integer value) {
        this.value = value;
    }

    boolean isDivisibleBy(int number) {
        return value % number == 0;
    }
}