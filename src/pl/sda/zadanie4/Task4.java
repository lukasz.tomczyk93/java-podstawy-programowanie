package pl.sda.zadanie4;

import java.util.Scanner;

class Task4 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Provide a positive number:");
        var number = scanner.nextInt();

        if (number <= 0) {
            System.out.println("Number cannot be less or equal 0");
            return;
        }

        System.out.println();
        solvedByForLoop(number);
    }

    private static void solvedByForLoop(int number) {
        for (int i = 1; i <= number; i++) {
            checkDivisibilityAndPrint(i);
        }
    }

    private static void checkDivisibilityAndPrint(int number) {
        var sdaNumber = new SdaNumber(number);

        if (sdaNumber.isDivisibleBy(9) && sdaNumber.isDivisibleBy(7)) {
            System.out.println("Pif Paf");
        } else if (sdaNumber.isDivisibleBy(3)) {
            System.out.println("Pif");
        } else if (sdaNumber.isDivisibleBy(7)) {
            System.out.println("Paf");
        } else {
            System.out.println(number);
        }
    }
}