package pl.sda.zadanie7;

import java.util.Scanner;

class Task7 {

    public static void main(String[] args) {
        System.out.print("Provide a positive number: ");
        var scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        if (number <= 0) {
            System.out.println("Number must be positive");
            return;
        }

        printFib(number);
    }

    private static void printFib(int number) {
        if (number < 1) {
            System.out.println("Number must be greater or equal 1");
            return;
        }

        System.out.println(new Fib(number));
    }
}