package pl.sda.zadanie7;

class Fib {

    private final Integer number;
    private final Integer value;

    Fib(Integer number) {
        this.number = number;
        value = calculateFib();
    }

    private Integer calculateFib() {
        int previousFib = 0;
        int nextFib = 1;
        int newFib = 1;

        for (int i = 1; i < number; i++) {
            newFib = previousFib + nextFib;
            previousFib = nextFib;
            nextFib = newFib;
        }

        return newFib;
    }

    @Override
    public String toString() {
        return String.format("fib(%s) = %s", number, value);
    }
}