package pl.sda.zadanie16;

import java.util.Scanner;

class Task16 {

    public static void main(String[] args) {

        int[] numbers = getUserInput();

        if (numbers.length == 0) {
            System.out.println("You have to provide exactly 10 numbers!");
            return;
        }

        int maxSubsequenceLength = getMaxSubsequenceLength(numbers);
        System.out.println(String.format("Max subsequence length: %s", maxSubsequenceLength));
    }

    private static int[] getUserInput() {
        int[] numbers = new int[10];
        System.out.print("Enter ten int numbers (split by ','): ");
        var scanner = new Scanner(System.in);

        String rawInput = scanner.nextLine();
        String[] numbersAsStrings = rawInput.split(",");

        if (numbersAsStrings.length != 10) {
            //Can be replaced by exception: throw new IllegalStateException("You have to provide exactly 10 numbers!");
            return new int[]{};
        }

        for (int i = 0; i < numbersAsStrings.length; i++) {
            numbers[i] = Integer.parseInt(numbersAsStrings[i]);
        }

        return numbers;
    }

    private static int getMaxSubsequenceLength(int[] numbers) {
        int currentIncreasingSubsequenceLength = 1;
        int maxSubsequenceLength = 1;
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i - 1] < numbers[i]) {
                currentIncreasingSubsequenceLength++;
            } else {
                currentIncreasingSubsequenceLength = 1;
            }

            if (currentIncreasingSubsequenceLength > maxSubsequenceLength) {
                maxSubsequenceLength = currentIncreasingSubsequenceLength;
            }
        }

        return maxSubsequenceLength;
    }
}