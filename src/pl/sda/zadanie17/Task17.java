package pl.sda.zadanie17;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

class Task17 {

    public static void main(String[] args) {
        var nextClassDate = getNextClassDate();
        var today = LocalDate.now();
        calculatePeriodInDays(nextClassDate, today);
    }

    private static LocalDate getNextClassDate() {
        var scanner = new Scanner(System.in);
        System.out.print("Provide your next class date: ");

        var nextClassDateAsString = scanner.next();
        //var nextClassDate = LocalDate.parse(nextClassDateAsString);
        var nextClassDate = LocalDate.parse(nextClassDateAsString, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        return nextClassDate;
    }

    private static void calculatePeriodInDays(LocalDate nextClassDate, LocalDate today) {
        Period between = Period.between(today, nextClassDate);
        //between.get(ChronoUnit.DAYS);
        int daysLeftToNextClass = between.getDays();
        System.out.println(String.format("Number of days that left to the next class: %s", daysLeftToNextClass));
    }
}
