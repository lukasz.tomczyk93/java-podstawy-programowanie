package pl.sda.zadanie19;

class Task19 {

    public static void main(String[] args) {
        var firstAuthor = new Author("Author A", "Nationality A");
        var secondAuthor = new Author("Author B", "Nationality B");

        var firstPoem = new Poem(firstAuthor, 12);
        var secondPoem = new Poem(firstAuthor, 7);
        var thirdPoem = new Poem(secondAuthor, 21);

        Poem[] poems = new Poem[]{firstPoem, secondPoem, thirdPoem};
        MaxResult maxResult = getMaxResult(poems);

        if (maxResult.authorOfPoemWithMaxStrophes() != null) {
            System.out.println(String.format(
                    "Author of poem with max number of strophes (%s) is: %s",
                    maxResult.maxNumberOfStrophes(),
                    maxResult.authorOfPoemWithMaxStrophes()
            ));
        }
    }

    private static MaxResult getMaxResult(Poem[] poems) {
        int maxNumberOfStrophes = 0;
        Author authorOfPoemWithMaxStrophes = null;
        for (Poem poem : poems) {
            if (poem.getStropheNumbers() > maxNumberOfStrophes) {
                maxNumberOfStrophes = poem.getStropheNumbers();
                authorOfPoemWithMaxStrophes = poem.getCreator();
            }
        }

        return new MaxResult(maxNumberOfStrophes, authorOfPoemWithMaxStrophes);
    }

    private record MaxResult(Integer maxNumberOfStrophes, Author authorOfPoemWithMaxStrophes) {
    }
}