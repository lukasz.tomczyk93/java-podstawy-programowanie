package pl.sda.zadanie19;

class Poem {

    private final Author creator;
    private final Integer stropheNumbers;

    Poem(Author creator, Integer stropheNumbers) {
        this.creator = creator;
        this.stropheNumbers = stropheNumbers;
    }

    Author getCreator() {
        return creator;
    }

    Integer getStropheNumbers() {
        return stropheNumbers;
    }
}