package pl.sda.zadanie18;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Task18 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Provide text: ");
        var userText = scanner.nextLine();

        //Pattern compiledPattern = Pattern.compile("[a]+[ ]psik");
        Pattern compiledPattern = Pattern.compile("[a]+[ ]psik", Pattern.CASE_INSENSITIVE);
        Matcher matcher = compiledPattern.matcher(userText);
        boolean userSneezed = matcher.find();
        if (userSneezed) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
    }
}