package pl.sda.zadanie2;

import java.util.Scanner;

class Task2 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.println("Provide your weight:");
        var providedWeight = scanner.nextFloat();

        System.out.println("Provide your height (in cm):");
        var providedHeight = scanner.nextInt();

        var bmi = new BMI(providedWeight, providedHeight);
        System.out.println(bmi);

        if (bmi.isOptimal()) {
            System.out.println("BMI optymalne");
        } else {
            System.out.println("BMI nieoptymalne");
        }
    }
}