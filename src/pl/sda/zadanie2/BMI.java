package pl.sda.zadanie2;

class BMI {

    private final float weight;
    private final int height;
    private final float value;

    BMI(float weight, int height) {
        this.weight = weight;
        this.height = height;
        value = calculateBmi();
    }

    private float calculateBmi() {
        float heightInMeters = ((float) height) / 100;
        return weight / (heightInMeters * heightInMeters);
    }

    boolean isOptimal() {
        return value >= 18.5 && value <= 24.9;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}