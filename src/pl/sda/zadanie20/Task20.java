package pl.sda.zadanie20;

import java.util.Random;
import java.util.Scanner;

class Task20 {

    public static void main(String[] args) {
        var random = new Random();
        int randomNumber = random.nextInt(101); //0 - 100
        var scanner = new Scanner(System.in);
        int providedNumber = -1;

        while (providedNumber != randomNumber) {
            System.out.print("Enter a number: ");
            providedNumber = scanner.nextInt();

            if (providedNumber != -1) {
                if (providedNumber > randomNumber) {
                    System.out.println("Za duzo");
                } else if (providedNumber < randomNumber) {
                    System.out.println("Za malo");
                }
            }
        }
        System.out.println("Bingo!");
    }
}