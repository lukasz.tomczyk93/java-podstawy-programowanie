package pl.sda.zadanie5;

import java.util.Scanner;

class Task5 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Provide a positive number:");
        var number = scanner.nextInt();

        if (number <= 0) {
            System.out.println("Number cannot be less or equal 0");
            return;
        }

        if (number == 1) {
            System.out.println("No prime numbers");
            return;
        }

        NumberUtils.printPrimeNumberSmallerThan(number);
    }
}
