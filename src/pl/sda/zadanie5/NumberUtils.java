package pl.sda.zadanie5;

class NumberUtils {

    public static void printPrimeNumberSmallerThan(int value) {
        if (value == 1) {
            System.out.println("No prime numbers");
            return;
        }

        for (int i = 1; i < value; i++) {
            if (isPrime(i)) {
                System.out.println(i);
            }
        }
    }

    private static boolean isPrime(int value) {
        if (value == 1) {
            return false;
        }

        int divisors = 1;
        for (int i = 2; i <= value; i++) {
            if ((value % i) == 0) {
                divisors++;
            }
        }

        return divisors == 2;
    }
}