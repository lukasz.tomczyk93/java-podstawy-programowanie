package pl.sda.zadanie1;


import java.util.Scanner;

class Task1 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Provide the radius:");

        float providesRadius = scanner.nextFloat();

        var circle = new Circle(providesRadius);
        double calculatedDiameter = circle.calculateDiameter();

        System.out.println(String.format("Diameter is %s", calculatedDiameter));
    }
}