package pl.sda.zadanie1;

class Circle {

    private final float radius;

    Circle(float radius) {
        this.radius = radius;
    }

    double calculateDiameter() {
        return 2 * Math.PI * radius;
    }
}