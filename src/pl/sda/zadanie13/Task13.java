package pl.sda.zadanie13;

import java.util.Scanner;

class Task13 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter a free text: ");

        String freeText = scanner.nextLine();
        String[] words = freeText.split(" ");

        for (String word : words) {
            System.out.print(String.format("%s %s ", word, word));
        }
    }
}