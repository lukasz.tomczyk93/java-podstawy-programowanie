package pl.sda.zadanie9;

import java.util.Scanner;

class Task9 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Provide a positive number: ");
        int waveLength = scanner.nextInt();

        if (waveLength <= 0) {
            System.out.println("Not a positive number");
            return;
        }

        printWaveNestedForLoop(waveLength);
    }

    private static void printWaveNestedForLoop(int waveLength) {
        String line_1 = "*      *";
        String line_2 = " *    * ";
        String line_3 = "  *  *  ";
        String line_4 = "   **   ";

        String[] lines = {line_1, line_2, line_3, line_4};

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < waveLength; j++) {
                System.out.print(lines[i]);
            }
            System.out.println();
        }
    }

    private static void printWaveUsingForLoopRepetitions(int waveLength) {
        String line_1 = "*      *";
        String line_2 = " *    * ";
        String line_3 = "  *  *  ";
        String line_4 = "   **   ";

        repeatLine(waveLength, line_1);
        repeatLine(waveLength, line_2);
        repeatLine(waveLength, line_3);
        repeatLine(waveLength, line_4);
    }

    private static void repeatLine(int waveLength, String line) {
        for (int i = 0; i < waveLength; i++) {
            System.out.print(line);
        }
        System.out.println();
    }
}