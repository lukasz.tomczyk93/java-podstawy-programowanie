package pl.sda.zadanie15;

import java.util.Scanner;

class Task15 {

    public static void main(String[] args) {
        int[] numbers = getUserInput();

        for (int i = 0; i < numbers.length; i++) {
            int currentNumber = numbers[i];
            int occurrences = getNumberOccurrences(currentNumber, numbers);
            boolean numberWasNotChecked = !numberWasChecked(currentNumber, i, numbers);
            if (numberWasNotChecked && occurrences >= 2) {
                System.out.println(currentNumber);
            }
        }
    }

    private static int[] getUserInput() {
        int[] numbers = new int[10];

        var scanner = new Scanner(System.in);
        for (int i = 0; i < 10; i++) {
            System.out.print("Enter int number: ");
            numbers[i] = scanner.nextInt();
        }

        return numbers;
    }

    private static int getNumberOccurrences(int number, int[] numbers) {
        int result = 0;
        for (int value : numbers) {
            if (number == value) {
                result++;
            }
        }

        return result;
    }

    private static boolean numberWasChecked(int currentNumber, int currentNumberIndex, int[] numbers) {
        boolean alreadyOccurred;
        for (int i = currentNumberIndex + 1; i < numbers.length; i++) {
            alreadyOccurred = currentNumber == numbers[i];
            if (alreadyOccurred) {
                return true;
            }
        }
        return false;
    }
}