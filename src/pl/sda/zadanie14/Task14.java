package pl.sda.zadanie14;

import java.util.Scanner;

class Task14 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter first character: ");
        char firstChar = scanner.next().charAt(0);

        if (characterIsOutOfRange(firstChar)) {
            System.out.println("Invalid argument");
            return;
        }

        System.out.print("Enter second character: ");
        char secondChar = scanner.next().charAt(0);

        if (characterIsOutOfRange(secondChar)) {
            System.out.println("Invalid argument");
            return;
        }

        int result = Math.abs(firstChar - secondChar) - 1;

        System.out.println(String.format("Result=%s", result));
    }

    private static boolean characterIsOutOfRange(char firstChar) {
        return firstChar < 'a' || firstChar > 'z';
    }
}