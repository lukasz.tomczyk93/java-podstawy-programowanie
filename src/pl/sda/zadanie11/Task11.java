package pl.sda.zadanie11;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class Task11 {

    public static void main(String[] args) throws IOException {
        var bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        var exitText = "Starczy";

        int maxTextLength = 0;
        String input;

        do {
            System.out.print("Enter text (to quit type 'starczy'): ");
            input = bufferedReader.readLine();
            if (input.isBlank()) {
                System.out.println("Text not provided");
                break;
            }

            int currentTextLength = input.length();
            if (!input.equalsIgnoreCase(exitText) && currentTextLength > maxTextLength) {
                maxTextLength = currentTextLength;
            }

        } while (!input.equalsIgnoreCase(exitText));

        System.out.println(String.format("Max: %x", maxTextLength));
    }
}