package pl.sda.zadanie3;

class ZeroOfFunction {

    private final Double value;

    ZeroOfFunction(Double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}