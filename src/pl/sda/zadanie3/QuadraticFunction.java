package pl.sda.zadanie3;

class QuadraticFunction {

    private final Integer a;
    private final Integer b;
    private final Integer c;
    private final Integer delta;

    QuadraticFunction(Integer a, Integer b, Integer c) {
        this.a = a;
        this.b = b;
        this.c = c;
        delta = calculateDelta();
    }

    private Integer calculateDelta() {
        return b * b - 4 * a * c;
    }

    ZeroOfFunction[] getZeros() {
        if (delta < 0) {
            return new ZeroOfFunction[]{};
        } else if (delta == 0) {
            return new ZeroOfFunction[]{
                    new ZeroOfFunction(getX1())
            };
        } else {
            return new ZeroOfFunction[]{
                    new ZeroOfFunction(getX1()),
                    new ZeroOfFunction(getX2()),
            };
        }
    }

    private double getX2() {
        return (-1 * b + Math.sqrt(delta)) / (2 * a);
    }

    private double getX1() {
        return (-1 * b - Math.sqrt(delta)) / (2 * a);
    }
}