package pl.sda.zadanie3;

import java.util.Arrays;
import java.util.Scanner;

class Task3 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("a:");
        int a = scanner.nextInt();

        System.out.print("b:");
        int b = scanner.nextInt();

        System.out.print("c:");
        int c = scanner.nextInt();

        var quadraticFunction = new QuadraticFunction(a, b, c);
        ZeroOfFunction[] zeros = quadraticFunction.getZeros();

        if (zeros.length == 0) {
            System.out.println("negative delta - no zeros");
            return;
        }

        System.out.println(Arrays.toString(zeros));
    }
}
