package pl.sda.zadanie6;

class HarmonicSeries {

    private final Integer n;
    private final Double sum;

    HarmonicSeries(Integer n) {
        this.n = n;
        sum = calculateSum();
    }

    private Double calculateSum() {
        double sum = 0;
        for (int i = 1; i <= n; i++) {
            double element = 1.00 / ((double) i);
            sum = sum + element;
        }
        return sum;
    }

    Double getSum() {
        return sum;
    }
}
