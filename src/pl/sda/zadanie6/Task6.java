package pl.sda.zadanie6;

import java.util.Scanner;

class Task6 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Provide a positive number:");
        var n = scanner.nextInt();
        var harmonicSeries = new HarmonicSeries(n);
        System.out.println(String.format("Harmonic series: %s", harmonicSeries.getSum()));
    }
}