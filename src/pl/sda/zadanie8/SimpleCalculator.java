package pl.sda.zadanie8;

class SimpleCalculator {

    private final Float firstNumber;
    private final Float secondNumber;
    private final String operation;

    SimpleCalculator(Float firstNumber, Float secondNumber, String operation) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
        this.operation = operation;
    }

    boolean isOperationSupported(String operation) {
        return operation.equals("+")
                || operation.equals("-")
                || operation.equals("*")
                || operation.equals("/");
    }

    double performCalculation() {
        return switch (operation) {
            case "*" -> firstNumber * secondNumber;
            case "/" -> handleDivision(firstNumber, secondNumber);
            case "+" -> secondNumber + firstNumber;
            case "-" -> firstNumber - secondNumber;
            default -> Double.MIN_VALUE; //Can be replaced by throw new IllegalStateException("Unsupported operation");
        };
    }

    private double handleDivision(float firstNumber, float secondNumber) {
        if (secondNumber != 0) {
            return firstNumber / secondNumber;
        }
        System.out.println("You cannot divide by 0");
        //Can be replaced by throw new IllegalStateException("You cannot divide by 0")
        return Double.MIN_VALUE;
    }
}