package pl.sda.zadanie8;

import java.util.Scanner;

class Task8 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);

        System.out.print("Provide the first number: ");
        float firstNumber = scanner.nextFloat();

        System.out.print("Provide an option: ");
        String operation = scanner.next();

        System.out.print("Provide the second number: ");
        float secondNumber = scanner.nextFloat();

        var simpleCalculator = new SimpleCalculator(firstNumber, secondNumber, operation);

        if (!simpleCalculator.isOperationSupported(operation)) {
            System.out.println("Unsupported operation");
            return;
        }

        double result = simpleCalculator.performCalculation();
        if (result == Double.MIN_VALUE) {
            return;
        }

        System.out.println(String.format("%s %s %s = %s", firstNumber, operation, secondNumber, result));
    }
}