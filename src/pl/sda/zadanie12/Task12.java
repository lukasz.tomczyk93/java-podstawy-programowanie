package pl.sda.zadanie12;

import java.util.Scanner;

class Task12 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter a free text: ");

        String freeText = scanner.nextLine();
        char[] chars = freeText.toCharArray();

        int numberOfAllCharacters = chars.length;
        int numberOfSpaces = 0;
        for (char aChar : chars) {
            if (String.valueOf(aChar).equals(" ")) {
                numberOfSpaces++;
            }
        }

        double spacesPercentage = ((double) numberOfSpaces / (double) numberOfAllCharacters) * 100;

        System.out.println(String.format("Spaces part was %s%%", spacesPercentage));
    }
}